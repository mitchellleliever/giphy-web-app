
# giphy-web-app

## Run Locally


To run the web app locally, run the following commands from the project directory:

```bash
cd app
```

```bash
npm install
```

```bash
npm run dev
```

Open your browser on `http://localhost:3000`


Tested with Node V16.17.0