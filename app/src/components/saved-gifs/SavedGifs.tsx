import React from "react";

import GifsCollection from "../gifs/browsing-gifs/GifsCollection";
import { useUserPrefsContext } from "../user-prefs/userPrefsContext";
import NoGifsFound from "../gifs/browsing-gifs/NoGifsFound";
import { useSavedGifs } from "../../hooks/gifHooks";

const SavedGifs: React.FC = () => {
  const { state } = useUserPrefsContext();
  const [loadedLocalStorage, setLoadedLocalStorage] = React.useState(false);
  const gifsPaginationLimit = 48;
  const { paginationState, loadGifs } = useSavedGifs(gifsPaginationLimit);

  React.useEffect(() => {
    if (paginationState.gifs.length > 0) {
      return;
    }

    if (state.savedGifIds && state.savedGifIds.length > 0) {
        loadGifs(0, state.savedGifIds);
    }
    
    setLoadedLocalStorage(true);
  }, [state.savedGifIds]);

  return state.savedGifIds.length === 0 && loadedLocalStorage ? (
    <NoGifsFound />
  ) : (
    <GifsCollection
      gifs={paginationState.gifs}
      paginationLimit={gifsPaginationLimit}
      moreToLoad={paginationState.moreToLoad}
      error={paginationState.error}
    />
  );
};

export default SavedGifs;
