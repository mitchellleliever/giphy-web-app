import React from "react";

import UserPrefsContextProvider from "../user-prefs/UserPrefsContextProvider";
import SavedGifs from "./SavedGifs";

const SavedGifsPage: React.FC = () => {

  return (
    <div className="pt-8">
      <UserPrefsContextProvider>
        <p className="text-4xl font-bold pb-6 mx-4">Saved GIFs</p>
        <SavedGifs />
      </UserPrefsContextProvider>
    </div>
  );
};

export default SavedGifsPage;