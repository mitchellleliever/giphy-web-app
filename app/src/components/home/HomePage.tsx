import React from "react";

import SearchBox from "../shared/Search";
import HomePageGifs from "./HomePageGifs";
import UserPrefsContextProvider from "../user-prefs/UserPrefsContextProvider";

const HomePage: React.FC = () => {
  const [searchTerm, setSearchTerm] = React.useState("");
  const [searchInput, setSearchInput] = React.useState("");

  return (
    <div className="pt-8">
      <div className="mx-4 flex flex-wrap">
        <p className="text-4xl font-bold pb-6">Home</p>
        <div className="flex ml-0 mr-auto lg:ml-auto lg:mr-0 my-auto">
          <SearchBox
            placeholder={"Search GIFs..."}
            onChange={(val) => setSearchInput(val)}
            onSubmit={() => setSearchTerm(searchInput)}
          />
          <button className="px-4 py-2 transition bg-blue-700 hover:brightness-90 rounded-lg ml-2 text-white" onClick={() => setSearchTerm(searchInput)}>
            Search
          </button>
        </div>
      </div>
      <UserPrefsContextProvider>
        <HomePageGifs searchTerm={searchTerm} />
      </UserPrefsContextProvider>
    </div>
  );
};

export default HomePage;