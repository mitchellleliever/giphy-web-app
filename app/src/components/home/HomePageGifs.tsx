import React from "react";

import { useTrendingGifs } from "../../hooks/gifHooks";
import NoGifsFound from "../gifs/browsing-gifs/NoGifsFound";
import GifsCollection from "../gifs/browsing-gifs/GifsCollection";

interface IHomePageGifs {
  searchTerm: string;
}

const HomePageGifs: React.FC<IHomePageGifs> = ({ searchTerm }) => {
  const gifsPaginationLimit = 48;
  const { paginationState, resetResults, loadGifs } = useTrendingGifs(
    gifsPaginationLimit,
    searchTerm
  );

  React.useEffect(() => {
    loadGifs(0, searchTerm);
  }, [searchTerm]);

  return resetResults ? (
    <></>
  ) : searchTerm !== "" && paginationState.gifs.length === 0 ? (
    <NoGifsFound />
  ) : (
    <GifsCollection
      gifs={paginationState.gifs}
      paginationLimit={gifsPaginationLimit}
      moreToLoad={paginationState.moreToLoad}
      error={paginationState.error}
    />
  );
};

export default HomePageGifs;
