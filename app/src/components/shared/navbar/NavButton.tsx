import React from "react";
import Link from "next/link";

import { classNames } from "../../../helpers/generalHelpers";

interface INavButton {
    text: string
    url: string
    active: boolean
}

const NavButton: React.FC<INavButton> = ({ url, text, active }) => {
    return (
        <Link href={url} className="block px-5 py-3 transition hover:opacity-80">
            <p className={classNames("text-xl font-bold", active ? "underline" : "")}>{text}</p>
        </Link>
    )
}

export default NavButton;