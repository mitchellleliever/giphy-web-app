import React from "react"

import NavButton from "./NavButton"

type Page = 'home' | 'saved-gifs'

const NavBar: React.FC<{ activePage: Page }> = ({ activePage }) => {
    return (
        <div className="flex pt-5">
            <div className="flex mx-auto space-x-5">
                <NavButton text="Home" url="/" active={activePage === 'home'} />
                <NavButton text="Saved GIFs" url="/saved-gifs" active={activePage === 'saved-gifs'} />
            </div>
        </div>
    )
}

export default NavBar