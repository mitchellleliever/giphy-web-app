import React from "react";

interface ISearchBox {
  placeholder?: string
  onChange?: (value: string) => void;
  onSubmit?: () => void;
}

const SearchBox: React.FC<ISearchBox> = ({ placeholder, onChange, onSubmit }) => {
  const onKeyDown: React.KeyboardEventHandler = (
    event: React.KeyboardEvent
  ) => {
    if (event.code === "Enter" && onSubmit) {
      onSubmit();
    }
  };

  return (
    <input
      type="text"
      className="search-input"
      placeholder={placeholder}
      onChange={onChange ? (e) => onChange(e.target.value) : undefined}
      onKeyDown={onKeyDown}
    />
  );
};

export default SearchBox;
