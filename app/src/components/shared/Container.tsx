import React from "react";

const Container: React.FC<{ children?: React.ReactNode }> = ({ children }) => {
  return (
    <div className="w-full flex">
      <div className="mx-auto w-[1196px]">{children}</div>
    </div>
  );
};

export default Container;
