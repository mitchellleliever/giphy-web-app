import React from "react";

import { IUserPrefsState, UserPrefsUpdateAction } from "./userPrefsReducer";

interface IUserPrefsContextProps {
  state: IUserPrefsState;
  dispatch: React.Dispatch<UserPrefsUpdateAction>;
}

const UserPrefsContext = React.createContext<IUserPrefsContextProps>(
  {} as IUserPrefsContextProps
);

export function useUserPrefsContext() {
  return React.useContext(UserPrefsContext);
}

export default UserPrefsContext;
