import React from "react";

import {
  reducer,
  initialUserPrefs,
  SAVED_GIFS_LOCAL_STORAGE_KEY,
} from "./userPrefsReducer";
import UserPrefsContext from "./userPrefsContext";

const UserPrefsContextProvider: React.FC<{ children?: React.ReactNode }> = ({
  children,
}) => {
  const [state, dispatch] = React.useReducer(reducer, initialUserPrefs);

  React.useEffect(() => {
    if (window) {
      const savedGifIds = window.localStorage.getItem(
        SAVED_GIFS_LOCAL_STORAGE_KEY
      );
      if (savedGifIds) {
        dispatch({
          type: "set-initial-saved-gifs",
          ids: savedGifIds.split(","),
        });
      }
    }
  }, []);

  return (
    <UserPrefsContext.Provider value={{ state, dispatch }}>
      {children}
    </UserPrefsContext.Provider>
  );
};

export default UserPrefsContextProvider;
