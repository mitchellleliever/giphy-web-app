export const SAVED_GIFS_LOCAL_STORAGE_KEY = "savedGifIds";

export interface IUserPrefsState {
  savedGifIds: string[];
}
export type UserPrefsUpdateAction =
  | { type: "add-saved-gif"; id: string }
  | { type: "remove-saved-gif"; id: string }
  | { type: "set-initial-saved-gifs"; ids: string[] };

export const initialUserPrefs: IUserPrefsState = {
  savedGifIds: [],
};

export const reducer = (
  state: IUserPrefsState,
  action: UserPrefsUpdateAction
): IUserPrefsState => {
  const newSavedGifIds = [...state.savedGifIds];

  switch (action.type) {
    case "add-saved-gif":
      newSavedGifIds.push(action.id);
      newSavedGifIds.sort((a, b) => (a < b ? -1 : a > b ? 1 : 0));
      window.localStorage.setItem(
        SAVED_GIFS_LOCAL_STORAGE_KEY,
        newSavedGifIds.toString()
      );
      return { ...state, savedGifIds: newSavedGifIds };

    case "remove-saved-gif":
      newSavedGifIds.splice(state.savedGifIds.indexOf(action.id), 1);
      window.localStorage.setItem(
        SAVED_GIFS_LOCAL_STORAGE_KEY,
        newSavedGifIds.toString()
      );
      return { ...state, savedGifIds: newSavedGifIds };

    case "set-initial-saved-gifs":
      return { ...state, savedGifIds: action.ids };

    default:
      return state;
  }
};
