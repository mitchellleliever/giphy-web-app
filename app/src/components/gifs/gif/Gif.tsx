import React from "react";

import styles from '../../../styles/gifs.module.css'
import { GIFObject } from "giphy-api";
import { classNames } from "../../../helpers/generalHelpers";
import SaveButton from "./SaveButton";

interface IGif {
  gifData: GIFObject
  index: number
}

const Gif: React.FC<IGif> = ({ gifData, index }) => {
  const [loaded, setLoaded] = React.useState(false);
  const gifImageRef = React.useRef<HTMLImageElement>(null);
  const [height, setHeight] = React.useState<number | undefined>(undefined);

  React.useEffect(() => {
    if (gifImageRef.current) {
      const aspectRatio = parseInt(gifData.images.downsized.height) / parseInt(gifData.images.downsized.width);
      setHeight(Math.round(gifImageRef.current.width * aspectRatio));
    }
  }, [gifImageRef]);

  const colorIdx = index % 4;
  return (
    <div className={styles.gifParent}>
      {!loaded && (
        <div
          className="absolute top-0 left-0 bottom-0 right-0">
          <div
            className="rounded-lg pointer-events-none m-4"
            style={{
              backgroundColor: colorIdx === 0 ? 'red' : colorIdx === 1 ? 'blue' : colorIdx === 2 ? 'green' : 'purple',
              height: `${height}px`
            }}
          >
            <div className="flex h-full">
              <div className="m-auto spinner opacity-50"></div>
            </div>
          </div>
        </div>
      )}
      <img
        ref={gifImageRef}
        className={classNames(styles.gifImage, "rounded-lg border border-white/50")}
        src={gifData.images.downsized.url}
        width="100%"
        height="auto"
        style={{ minHeight: loaded ? "0" : `${height}px` }}
        alt={gifData.title}
        onLoad={() => setLoaded(true)}
      />
      {gifData.title && <p className={styles.gifTitle}>{gifData.title}</p>}
      {gifData.username && <p className={styles.gifAuthor}>{gifData.username}</p>}
      {loaded && <SaveButton gifId={gifData.id} />}
    </div>
  );
};

export default Gif;
