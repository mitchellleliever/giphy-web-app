import React from "react";

import { classNames } from "../../../helpers/generalHelpers";
import { HeartIcon } from "../../../components/shared/Icons";
import { useUserPrefsContext } from "../../../components/user-prefs/userPrefsContext";

const SaveButton: React.FC<{ gifId: string }> = ({ gifId }) => {
  const { state, dispatch } = useUserPrefsContext();
  const isSaved = () => state.savedGifIds.includes(gifId);

  const clickedSaveBtn = () => {
    if (isSaved()) {
      dispatch({ type: "remove-saved-gif", id: gifId });
    } else {
      dispatch({ type: "add-saved-gif", id: gifId });
    }
  };

  return (
    <div className="absolute -top-2 -right-2">
      <button
        className={classNames(
          "flex p-2 rounded-lg transition text-white",
          isSaved()
            ? "bg-red-500 hover:brightness-90"
            : "bg-black/20 hover:border-red-500 hover:bg-black/50"
        )}
        onClick={clickedSaveBtn}
      >
        <HeartIcon
          pixelSize={16}
          fillColor="white"
          parentClass="my-auto"
        />
      </button>
    </div>
  );
};

export default SaveButton;
