import React from "react";
import { GIFObject } from "giphy-api";

import Gif from "../gif/Gif";

const GifsColumn: React.FC<{ gifs: GIFObject[] }> = ({ gifs }) => {
  return (
    <div className="gifs-col">
      {gifs.map((gif, i) => {
        return <Gif key={i} gifData={gif} index={i} />;
      })}
    </div>
  );
};

export default GifsColumn;
