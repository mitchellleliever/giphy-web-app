import React from "react";

import GifsColumn from "./GifsColumn";
import { GIFObject } from "giphy-api";
import { classNames } from "../../../helpers/generalHelpers";

interface IGifsCollection {
    gifs: GIFObject[]
    paginationLimit: number
    moreToLoad: boolean
    error?: string
}

const GifsCollection: React.FC<IGifsCollection> = ({ gifs, paginationLimit, moreToLoad, error }) => {
  const paginations = Math.ceil(gifs.length / paginationLimit);

  return (
    <>
      <div className="flex flex-wrap">
        {Array(paginations)
          .fill(0)
          .map((_, paginationIdx) => {
            const gifsInBatch = gifs.slice(
              paginationIdx * paginationLimit,
              paginationIdx * paginationLimit + paginationLimit
            );
            return (
              <React.Fragment key={paginationIdx}>
                {Array(4)
                  .fill(0)
                  .map((_, colIdx) => {
                    return (
                      <GifsColumn
                        key={colIdx}
                        gifs={gifsInBatch.filter((_, i) => i % 4 === colIdx)}
                      />
                    );
                  })}
              </React.Fragment>
            );
          })}
      </div>
      {!error && (
        <div className={classNames("h-96 flex", !moreToLoad ? "opacity-0" : "")}>
          <div className="spinner spinner-black m-auto"></div>
        </div>
      )}
      {error && (
        <div className="w-full h-96 flex">
          <p className="text-2xl pt-8 font-bold text-red-600 text-center m-auto">
            {error}
          </p>
        </div>
      )}
    </>
  );
};

export default GifsCollection;
