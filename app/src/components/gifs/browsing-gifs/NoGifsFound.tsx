import React from "react";

const NoGifsFound: React.FC = () => {
  return (
    <div className="w-full">
      <div className="flex min-h-screen">
        <p className="text-4xl pt-8 font-bold m-auto">
          No GIFs found
        </p>
      </div>
    </div>
  );
};

export default NoGifsFound;
