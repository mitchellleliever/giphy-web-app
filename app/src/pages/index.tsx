import React from "react";
import Head from "next/head";

import Container from "../components/shared/Container";
import HomePage from "../components/home/HomePage";
import NavBar from "../components/shared/navbar/Navbar";

export default function Home() {
  return (
    <>
      <Head>
        <title>Home</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main>
        <Container>
          <NavBar activePage="home" />
          <HomePage />
        </Container>
      </main>
    </>
  );
}
