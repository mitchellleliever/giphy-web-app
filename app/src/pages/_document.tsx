import { Html, Head, Main, NextScript } from 'next/document'

import NavBar from '../components/shared/navbar/Navbar'

export default function Document() {
  return (
    <Html lang="en">
      <Head>
        <link href="/dist/output.css" rel="stylesheet" />
      </Head>
      <body>
        <Main />
        <NextScript />
      </body>
    </Html>
  )
}
