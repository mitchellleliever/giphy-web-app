import React from "react";
import Head from "next/head";

import Container from "../components/shared/Container";
import SavedGifsPage from "../components/saved-gifs/SavedGifsPage";
import NavBar from "../components/shared/navbar/Navbar";

export default function Home() {

  return (
    <>
      <Head>
        <title>Saved GIFs</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main>
        <Container>
          <NavBar activePage="saved-gifs" />
          <SavedGifsPage />
        </Container>
      </main>
    </>
  );
}
