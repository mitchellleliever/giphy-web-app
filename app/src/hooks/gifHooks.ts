import { useEffect, useReducer, useState } from "react";

import {
  getTrendingGifs,
  searchGifs,
  getSpecificGifs,
} from "../helpers/giphyApi";
import { useScrollPos } from "./generalHooks";
import { GIFObject, MultiResponse } from "giphy-api";
import { useUserPrefsContext } from "../components/user-prefs/userPrefsContext";

interface IPaginateGifsState {
  gifs: GIFObject[];
  moreToLoad: boolean;
  currentOffset: number;
  error?: string;
}

export function useTrendingGifs(paginationLimit: number, searchTerm: string) {
  const [paginationState, setPaginationState] = useState<IPaginateGifsState>({
    gifs: [],
    moreToLoad: true,
    currentOffset: 0,
  });
  const [fetching, setFetching] = useState(false);
  const [resetResults, setResetResults] = useState(false);
  const { scrollPos, scrollOffsetHeight } = useScrollPos();

  const handleApiResponse = (
    resp: Promise<MultiResponse>,
    offsetUsed: number
  ) => {
    setFetching(true);
    resp
      .then((response) => {
        setPaginationState({
          gifs:
            offsetUsed > 0
              ? paginationState.gifs.concat(response.data)
              : response.data,
          currentOffset: offsetUsed + response.pagination.count,
          moreToLoad:
            response.pagination.offset + response.pagination.count !==
            response.pagination.total_count,
          error: undefined,
        });
      })
      .catch((error) => {
        console.error(error);
        setPaginationState({
          ...paginationState,
          error: `There was an error: ${error}`,
        });
      })
      .finally(() => {
        setResetResults(false);
        setFetching(false);
      });
  };

  const loadGifs = (atOffset: number, searchTerm?: string) => {
    if (atOffset === 0 && !resetResults) {
      setResetResults(true);
    } else if (atOffset > 0 && resetResults) {
      setResetResults(false);
    }

    if (searchTerm) {
      handleApiResponse(
        searchGifs(searchTerm, atOffset, paginationLimit),
        atOffset
      );
    } else {
      handleApiResponse(getTrendingGifs(atOffset, paginationLimit), atOffset);
    }
  };

  useEffect(() => {
    if (
      paginationState.gifs.length > 0 &&
      paginationState.moreToLoad &&
      scrollOffsetHeight - 250 < scrollPos &&
      !fetching
    ) {
      loadGifs(paginationState.currentOffset, searchTerm);
    }
  }, [scrollPos]);

  return {
    paginationState,
    resetResults,
    loadGifs,
  };
}

export function useSavedGifs(paginationLimit: number) {
  const [paginationState, setPaginationState] = useState<IPaginateGifsState>({
    gifs: [],
    moreToLoad: true,
    currentOffset: 0,
  });
  const { state: userPrefsState } = useUserPrefsContext();
  const [fetching, setFetching] = useState(false);
  const { scrollPos, scrollOffsetHeight } = useScrollPos();

  const loadGifs = (offset: number, savedGifIds: string[]) => {
    setFetching(true);
    const endIndex = offset + paginationLimit;
    getSpecificGifs(
      savedGifIds.slice(offset, Math.min(savedGifIds.length, endIndex))
    )
      .then((response) => {
        setPaginationState({
          gifs: paginationState.gifs.concat(response.data),
          moreToLoad: savedGifIds.length > endIndex,
          currentOffset: paginationState.currentOffset + response.data.length,
        });
      })
      .catch((error) => {
        console.log(error);
        setPaginationState({
          ...paginationState,
          error,
        });
      })
      .finally(() => {
        setFetching(false);
      });
  };

  useEffect(() => {
    if (
      paginationState.gifs.length > 0 &&
      paginationState.moreToLoad &&
      scrollOffsetHeight - 250 < scrollPos &&
      !fetching
    ) {
      loadGifs(paginationState.currentOffset, userPrefsState.savedGifIds);
    }
  }, [scrollPos]);

  return {
    paginationState,
    loadGifs,
  };
}
