import { useEffect, useState } from "react";

export const useScrollPos = () => {
  const [scrollPos, setScrollPos] = useState(0);
  const [scrollOffsetHeight, setScrollOffsetHeight] = useState(0);

  const handleScroll = () => {
    setScrollPos(window.innerHeight + document.documentElement.scrollTop);
    setScrollOffsetHeight(document.documentElement.offsetHeight);
  };
  useEffect(() => {
    window.addEventListener("scroll", handleScroll);
    return () => window.removeEventListener("scroll", handleScroll);
  }, []);

  return { scrollPos, scrollOffsetHeight };
};
