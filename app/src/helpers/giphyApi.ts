const api_key = process.env.NEXT_PUBLIC_GIPHY_API_KEY;

const defaultLimit = 25;
import { MultiResponse } from "giphy-api";

export async function getTrendingGifs(
  offset?: number,
  limit?: number
): Promise<MultiResponse> {
  const response = await fetch(
    `https://api.giphy.com/v1/gifs/trending?api_key=${api_key}&limit=${limit ?? defaultLimit}&rating=g&offset=${offset ?? 0}`
  );
  const data = await response.json();
  return data;
}

export async function searchGifs(
  searchTerm: string,
  offset?: number,
  limit?: number
): Promise<MultiResponse> {
  const response = await fetch(
    `https://api.giphy.com/v1/gifs/search?api_key=${api_key}&q=${searchTerm}&limit=${limit ?? defaultLimit}&offset=${offset ?? 0}`
  );
  const data = await response.json();
  return data;
}

export async function getSpecificGifs(ids: string[]): Promise<MultiResponse> {
    const response = await fetch(
      `https://api.giphy.com/v1/gifs?api_key=${api_key}&ids=${ids.toString()}`
    );
    const data = await response.json();
    return data;
}